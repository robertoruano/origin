$(document).ready(function()
{
    $('.parallax').parallax();
});


function ValidateName(nombre)  
{  
    for (var i=0; i<nombre.length;i++)
    {
        if((nombre[i]>='a' && nombre[i]<='z')||(nombre[i]>='A' && nombre[i]<='Z')||(nombre[i]==' '));
        else
        {
            alert("En la casilla de nombre/nombre de usuario solo se aceptan letras");
            return false;
        }
    }
    return true;
}

function ValidateRFC(rfc)  
{  
    if(rfc.length==13)
    {
        for (var i=0; i<rfc.length;i++)
        {
            if((rfc[i]>='0' && rfc[i]<='9')||(rfc[i]>='A' && rfc[i]<='Z'));
            else
            {
                alert("Un RFC consta de 13 caracteres que pueden ser mayúsculas o números");
                return false;
            }
        }
        return true;
    }
    alert("Un RFC consta de 13 caracteres que pueden ser mayúsculas o números");
    return false;
}

function mayusculas(pass)
{
    for (var i=0; i<=pass.length; i++)
    {
        if (pass[i]>='A' && pass[i]<='Z')
        {
            return 1;
        }
    }
    return 0;
}


function minusculas(pass)
{
    for (var i=0; i<=pass.length; i++)
    {
        if (pass[i]>='a' && pass[i]<='z')
        {
            return 1;
        }
    }
    return 0;
}


function numero(pass)
{
    for (var i=0; i<=pass.length; i++)
    {
        if (pass[i]>='0' && pass[i]<='9')
        {
            return 1;
        }
    }
    return 0;
}


function especial(pass)
{
    for (var i=0; i<=pass.length; i++)
    {
        if (pass[i]=='!' || pass[i]=='@' || pass[i]=='#' || pass[i]=='$' || pass[i]=='%' || pass[i]=='&' || pass[i]=='*')
        {
            return 1;
        }
    }
    return 0;
}

function espacios(pass)
{
    for (var i=0; i<=pass.length; i++)
    {
        if (pass[i] ==' ')
        {
            return 0;
        }
    }
    return 1;
}

function ValidatePassword(password)
{
    if(espacios(password)!=0)
    {
        if(mayusculas(password)==0)
        {
            alert("La contraseña debe tener al menos una letra mayuscula");
            return false;
        }
        if(numero(password)==0)
        {
            alert("La contraseña debe tener al menos un numero");
            return false;
        }
        if(password.length < 8)
        {
            alert("La contraseña debe tener al menos ocho caracteres");
            return false;
        }
        if(minusculas(password)==0)
        {
            alert("La contraseña debe tener al menos una letra minuscula");
            return false;
        }
    }
    else
    {
        alert("La contraseña no debe de tener espacios");
        return false;
    }
    return true;
}

function Validar_Contaduria()
{
    var nombre = document.getElementById("first_name").value;
    var usuario = document.getElementById("client_user").value;
    var rfc = document.getElementById("rfc").value;
    var email = document.getElementById("email").value;
    var password = document.getElementById("password").value;
    var fechanac = document.getElementById("fnacimiento").value;
    var vnombre = false;
    var vapellido = false;
    var vrfc = false;
    var vpassword = false;
    //var md5password = md5(password);
    if (nombre.length!=0 && usuario.length!=0 && rfc.length!=0 && email.length!=0 && password.length!=0 && fechanac.length!=0)
    {
        vnombre = ValidateName(nombre);
        vapellido = ValidateName(usuario);
        vrfc = ValidateRFC(rfc);
        vpassword = ValidatePassword(password);
        if(vnombre==vapellido==vrfc==vpassword==true)
        {
            //password.value = md5password;
            return true;
        }
        else
        {
            return false; 
        }
    }
    else
    {
        alert("Uno o más campos vacíos");
        return false;
    }
    return false;
}

function Validar_Contaduria_Busqueda()
{
    var usuario = document.getElementById("usuario_sesion").value;
    var password = document.getElementById("password_sesion").value;
    
    if (usuario.length!=0 && password.length!=0)
    {
        return true;
    }
    else
    {
        alert("Uno o más campos vacíos");
        return false;
    }
    return false;
}

function Validar_Contaduria_Busqueda_Admin()
{
    var usuario = document.getElementById("admin_user").value;
    var password = document.getElementById("admin_pass").value;
    
    if (usuario.length!=0 && password.length!=0)
    {
        return true;
    }
    else
    {
        alert("Uno o más campos vacíos");
        return false;
    }
    return false;
}




function showUser(str) 
{
  if (str=="") 
  {
    document.getElementById("txtHint").innerHTML="";
    return;
  } 
  if (window.XMLHttpRequest) 
  {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp=new XMLHttpRequest();
  } 
  else 
  { // code for IE6, IE5
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange=function() 
  {
    if (xmlhttp.readyState==4 && xmlhttp.status==200) 
    {
      document.getElementById("txtHint").innerHTML=xmlhttp.responseText;
    }
  }
  xmlhttp.open("GET","getuser.php?q="+str,true);
  xmlhttp.send();
}
