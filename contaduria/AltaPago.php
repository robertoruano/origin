<html lang="es">
    <head>
        <meta charset="UTF-8">
    </head>
    <body>
        <?php
        
        function test_input($data) 
        {
           $data = trim($data);
           $data = stripslashes($data);
           $data = htmlspecialchars($data);
           return $data;
        }
        
        
        function numero($pass)
        {
            for ($i=0; $i<=strlen($pass); $i++)
            {
                if ($pass[$i]>='0' && $pass[$i]<='9')
                {
                    return 1;
                }
            }
            return 0;
        }
    
        
        if ($_SERVER["REQUEST_METHOD"] == "POST") 
        {
          
           if (empty($_POST["client_user"])) {
             $error = "El nombre de usuario es requerido";
           } 
           else 
           {
             $name = test_input($_POST["client_user"]);
             // check if name only contains letters and whitespace
             if (!preg_match("/^[a-zA-Z ]*$/",$name)) {
               $error = "Solo se aceptan letras y espacios en el nombre de usuario"; 
             }
             else
             {
                 $userver="ok";
             }
           }
           
           if (empty($_POST["monto"])) {
             $error = "El monto es requerido";
           } 
           else {
             $montover= "ok";
           }
           
           if (empty($_POST["fpago"])) {
             $error = "La fecha del pago es requerida";
           } 
           else {
             $fpagover="ok";
           }
           
           if (empty($_POST["concepto"])) {
             $error = "Concepto del Pago es requerido";
           } 
           else {
             $conceptover= "ok";
           }
           
           
           if ($userver=="ok" && $fpagover=="ok" && $montover=="ok" && $conceptover=="ok")
           {
                $servername = getenv('IP');
                $username = getenv('C9_USER');
                $password = "";
                $dbname = "c9";
                $fpago = $_POST["fpago"];
                $usuario = $_POST["client_user"];
                $monto = $_POST["monto"];
                $concepto = $_POST["concepto"];
                
                // Create connection
                $conn = mysqli_connect($servername, $username, $password, $dbname);
                // Check connection
                if (!$conn) 
                {
                    die("Connection failed: " . mysqli_connect_error());
                }
                
                $sql = "SELECT pago_id FROM Pagos WHERE 1";
                $result= mysqli_query($conn, $sql);
                $pagoid= mysqli_num_rows($result);
                $pagoid= $pagoid+1;
                
                $sql = "SELECT * FROM Personas WHERE Usuario LIKE '$usuario'";
                $result= mysqli_query($conn, $sql);
                if (mysqli_num_rows($result)>0)
                {
                    $sql = "INSERT INTO Pagos (fecha, monto_total, concepto, usuario, pago_id)
                            VALUES ('$fpago', '$monto', '$concepto', '$usuario', '$pagoid')";
                            
                    if (mysqli_query($conn, $sql)) 
                    {
                        $error= "Los datos han sido dados de alta de forma existosa";
                        include("admin_pagos.html");
                    } 
                    else 
                    {
                        $error= "Lo sentimos, sus datos no han podido darse de alta. Intente más tarde";
                        include("admin_pagos.html");
                    }
                }
                mysqli_close($conn);
            }
            else
            {
                include("admin_pagos.html");
            }
        }
        ?>
    </body>
</html>
