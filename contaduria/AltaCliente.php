<html lang="es">
    <head>
        <meta charset="UTF-8">
    </head>
    <body>
        <?php
        
        function test_input($data) 
        {
           $data = trim($data);
           $data = stripslashes($data);
           $data = htmlspecialchars($data);
           return $data;
        }
        
        function mayusculas($pass)
        {
            for ($i=0; $i<=strlen($pass); $i++)
            {
                if ($pass[$i]>='A' && $pass[$i]<='Z')
                {
                    return 1;
                }
            }
            return 0;
        }
        
        function minusculas($pass)
        {
            for ($i=0; $i<=strlen($pass); $i++)
            {
                if ($pass[$i]>='a' && $pass[$i]<='z')
                {
                    return 1;
                }
            }
            return 0;
        }
        
        function numero($pass)
        {
            for ($i=0; $i<=strlen($pass); $i++)
            {
                if ($pass[$i]>='0' && $pass[$i]<='9')
                {
                    return 1;
                }
            }
            return 0;
        }
        
        function espacios($pass)
        {
            for ($i=0; $i<=strlen($pass); $i++)
            {
                if ($pass[$i] ==' ')
                {
                    return 0;
                }
            }
            return 1;
        }
        
        function fuerzapass($pass)
        {
            if(espacios($pass)!=0)
            {
                if(mayusculas($pass)==0)
                {
                    $passwordErr=("La contraseña debe tener al menos una letra mayuscula");
                    return $passwordErr;
                }
                if(numero($pass)==0)
                {
                    $passwordErr=("La contraseña debe tener al menos un numero");
                    return $passwordErr;
                }
                if(strlen($pass) < 8)
                {
                    $passwordErr=("La contraseña debe tener al menos ocho caracteres");
                    return $passwordErr;
                }
                if(minusculas($pass)==0)
                {
                    $passwordErr=("La contraseña debe tener al menos una letra minuscula");
                    return $passwordErr;
                }
                $passwordErr="";
                return $passwordErr;
            }
            else
            {
                $passwordErr=("La contraseña no debe tener espacios");
                return $passwordErr;
            }
        }
        
        if ($_SERVER["REQUEST_METHOD"] == "POST") 
        {
           if (empty($_POST["first_name"])) {
             $error = "El nombre es requerido";
           } 
           else 
           {
             $name = test_input($_POST["first_name"]);
             // check if name only contains letters and whitespace
             if (!preg_match("/^[a-zA-Z ]*$/",$name)) {
               $error = "Solo se aceptan letras y espacios en el nombre"; 
             }
             else
             {
                 $namever="ok";
             }
           }
           
           if (empty($_POST["client_user"])) {
             $error = "El nombre de usuario es requerido";
           } 
           else 
           {
             $name = test_input($_POST["client_user"]);
             // check if name only contains letters and whitespace
             if (!preg_match("/^[a-zA-Z ]*$/",$name)) {
               $error = "Solo se aceptan letras y espacios en el nombre de usuario"; 
             }
             else
             {
                 $userver="ok";
             }
           }
           
           if (empty($_POST["email"])) {
             $error = "Email es requerido";
           } 
           else {
             $email = test_input($_POST["email"]);
             // check if e-mail address is well-formed
             if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
               $error = "Formato de Email inválido"; 
             }
             else{
                 $emailver="ok";
             }
           }
           
           if (empty($_POST["password"])) {
             $error = "El password es requerido";
           } 
           else {
             $password = test_input($_POST["password"]);
             $passwordErr=fuerzapass($password);
             if($passwordErr=="")
             {
                $passwordver="ok";
             }
             else
             {
                 $error=$passwordErr;
             }
           }
           
           if (empty($_POST["fnacimiento"])) {
             $error = "La fecha de nacimiento es requerida";
           } 
           else {
             $fnacimientover="ok";
           }
           
           if ($namever == "ok" && $emailver == "ok" && $passwordver == "ok" && $userver=="ok" && $fnacimientover=="ok")
           {
                $servername = getenv('IP');
                $username = getenv('C9_USER');
                $password = "";
                $dbname = "c9";
                $nombre = $_POST["first_name"];
                $fnacimiento = $_POST["fnacimiento"];
                $usuario = $_POST["client_user"];
                $RFC = $_POST["rfc"];
                $passworduser = $_POST["password"];
                
                $passwordusermd5 = md5($passworduser);
                // Create connection
                $conn = mysqli_connect($servername, $username, $password, $dbname);
                // Check connection
                if (!$conn) 
                {
                    die("Connection failed: " . mysqli_connect_error());
                }
                
                $sqluser = "SELECT * FROM Personas WHERE Usuario LIKE '$usuario'";
                $sqlrfc = "SELECT * FROM Personas WHERE RFC LIKE '$RFC'";
                $resultuser= mysqli_query($conn, $sqluser);
                $resultrfc= mysqli_query($conn, $sqlrfc);
                if (mysqli_num_rows($resultuser)>0)
                {
                    $error= "Ese nombre de usuario ya existe";
                    include_once("admin.html");
                }
                else if (mysqli_num_rows($resultrfc)>0)
                {
                    $error= "Ya existe un usuario con ese RFC";
                    include("admin.html");
                }
                else
                {
                    $sql = "INSERT INTO Personas (Nombre, FNacimiento, RFC, Usuario, Password)
                            VALUES ('$nombre', '$fnacimiento', '$RFC', '$usuario', '$passwordusermd5')";
                            
                    if (mysqli_query($conn, $sql)) 
                    {
                        $error= "Los datos han sido dados de alta de forma existosa";
                        include("admin.html");
                    } 
                    else 
                    {
                        $error= "Lo sentimos, sus datos no han podido darse de alta. Intente más tarde";
                        include("admin.html");
                    }
                    
                }
                mysqli_close($conn);
            }
            else
            {
                include("admin.html");
            }
        }
        ?>
    </body>
</html>
